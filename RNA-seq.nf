
#Copyright or © or Copr. jean-pascal meneboo Université de lille
#jean-pascal.meneboo@univ-lille.fr
#
#This software is a computer program whose purpose is to RNA-seq analysis.
#
#This software is governed by the CeCILL license under French law and
#abiding by the rules of distribution of free software.  You can  use,
#modify and/ or redistribute the software under the terms of the CeCILL
#license as circulated by CEA, CNRS and INRIA at the following URL
#"http://www.cecill.info".

#As a counterpart to the access to the source code and  rights to copy,
#modify and redistribute granted by the license, users are provided only
#with a limited warranty  and the software's author,  the holder of the
#economic rights,  and the successive licensors  have only  limited
#liability.

#In this respect, the user's attention is drawn to the risks associated
#with loading,  using,  modifying and/or developing or reproducing the
#software by the user in light of its specific status of free software,
#that may mean  that it is complicated to manipulate,  and  that  also
#therefore means  that it is reserved for developers  and  experienced
#professionals having in-depth computer knowledge. Users are therefore
#encouraged to load and test the software's suitability as regards their
#requirements in conditions enabling the security of their systems and/or
#data to be ensured and,  more generally, to use and operate it in the
#same conditions as regards security.

#The fact that you are presently reading this means that you have had
#knowledge of the CeCILL license and that you accept its terms.


#!/usr/bin/env nextflow

/*
*params input
*/
params.reads = "$baseDir/data/*{1,2}.fastq"
params.genome = "$baseDir/data/GRCh37_region1.fa"
params.annotation = "$baseDir/data/GRCh37_region1.gtf"
params.output_dir = "data"
params.index = null




//print usage
if (params.help) {
    log.info ''
    log.info 'RNA-seq'
    log.info '-----------------------'
    log.info '.'
    log.info ''
    log.info 'Usage: '
    log.info '    nextflow RNA-seq.nf --genome PATH/genome.fa --reads "PATH/*{1,2}.fq" '
    log.info ' Or '
    log.info ' nextflow RNA-seq.nf --genome PATH/genome.fa  --index "PATH/genome*" --reads "PATH/*{1,2}.fq" '
    log.info ''
    log.info 'Options:'
    log.info '    --help                              Show this message and exit.'
    log.info '    --reads                             File reads paired in fastq [ex : "data/*_{1,2}.{fastq,fq}"'
    log.info '    --genome GENOME_FILE                Reference genome file in fomrat fa .'
    log.info '    --annotation ANNOTATION_FILE        Annotation file in format GTF .'
    log.info '    --index GENOME_INDEX_FILE           Index file with the same prefix of genome reference '
    log.info '                                        (ex : --genome PATH/hg19.fa --index "PATH/hg19*").[Optional but most faster].'
    exit 1
}


/*
*Path of genome file and annotation file
*/
genome_file = file(params.genome)
annotation_file = file(params.annotation)


/*
*Channel for reads pairs
*/
Channel
    .fromFilePairs( params.reads )
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
    .set { read_pairs }

read_pairs.into{
  read_pairs_qc
  read_pairs_map
  read_pairs_trim
}


/*
* trimming read
*OPTION :
*
*--illumina     Adapter sequence to be trimmed is the first 13bp of the Illumina
*       universal adapter  AGATCGGAAGAGC instead of the default auto-detection of adapter sequence.
*--gzip     Compress the output file with gzip.
*--paired   This option performs length trimming of quality/adapter/RRBS trimmed
*       reads for paired-end files
 */


process trim_galore {
    scratch '/home/jp/RNA-seq/test'
    tag "$pair_id"
    publishDir "result/RNA-seq/$params.output_dir/$pair_id/trim/", mode: "copy"

    input:
    set val(pair_id), file(reads) from read_pairs_trim

    output:
    set val(pair_id), file('*_val*') into read_files_trimmed
    file '*_trimming_report.txt' into trimgalore_results

    """
    trim_galore --paired --stringency 3 --gzip $reads
    """
}


//read_files_trimmed.subscribe { println it.text}
read_files_trimmed.into{
    read_files_trimmed_bam
    read_files_trimmed_QC
}
/*
* Control Quality of original read_pairs by fastQC
*/

process fastQC {
    scratch '/home/jp/RNA-seq/test'
    tag{pair_id}
    publishDir "result/RNA-seq/$params.output_dir/$pair_id/QC/", mode: "move"

    input:
    set pair_id, file(reads) from read_files_trimmed_QC

    output:
    file '*fastqc*' into qc

    """
    fastqc -t  ${task.cpus} ${reads}
    """
}

/*
 * Step 1.0 : Builds the genome index required by the mapping process.
 */

if(params.index == null){


    process buildIndex {

        tag "$genome_file.baseName"

        input:
        file genome from genome_file

        output:
        file '*' into genome_index

        """
        bowtie2-build ${genome} $genome.baseName
        """
    }
}
 else{
    genome_index =  Channel
                .fromPath(params.index).toList()
}



/*
 * Step 1.1 : Builds the transcript index, required by the mapping process.
 */

process buildTranscriptomeIndex{
    scratch '/home/jp/RNA-seq/test'
    tag "$genome"
    publishDir "result/RNA-seq/$params.output_dir/$pair_id/bam/", mode: "copy"


    input:
    file genome from genome_file
    file index from genome_index
    file annotation from annotation_file

    output:
    file  '*_tr' into transcriptome_index

    """
    tophat2 -p ${task.cpus} --GTF ${annotation} \
    --transcriptome-index=${genome.baseName}_tr $genome.baseName
    """
}

/*
* Step 2. : Maps each read-pair by using Tophat2.1.1 mapper tool
*OPTION :
*
*-p     Use this many threads to align reads.
*-r     This is the expected (mean) inner distance between mate pairs.
*--gtf  Annotation file in GTF (see doc of tophat2.1.1)
*--transcriptome-index=     Name of annotation index (see doc of tophat2.1.1)
*--coverage-search      Enables the coverage based search for junctions. Use when coverage
*    search is disabled by default (such as for reads 75bp or longer), for maximum sensitivity.
*--microexon-search       With this option, the pipeline will attempt to find
*    alignments incident to micro-exons. Works only for reads 50bp or longer.
 */
process mapping {
    scratch '/home/jp/RNA-seq/test'
    tag "$pair_id"
    publishDir "result/RNA-seq/$params.output_dir/$pair_id/bam/", mode: "copy"

    input:
    file genome from genome_file
    file index from genome_index
    file index_tr from transcriptome_index
    set pair_id , file (reads) from read_files_trimmed_bam
    file annotation from annotation_file

    output:
    set pair_id, "tophat_out" into log_map
    set pair_id, "${pair_id}.bam" into bam

    """
    tophat2 -p ${task.cpus} -r 100 --GTF ${annotation} \
    --transcriptome-index=$index_tr \
    --microexon-search \
    $genome.baseName $reads

    mv tophat_out/accepted_hits.bam ./${pair_id}.bam
    """
}


bam.into{
    bam_count_gene
    bam_count_exon
    bam_count_transcript
}

/*
* Step 3. : Count the transcript by using the "featureCounts" tool
*OPTION :
*
*-T     Number of the threads. 1 by default.
*-p     Count fragments (read pairs) instead of individual reads
*-M     Multi-mapping reads will also be counted. For a multi-mapping read,
*       all its reported alignments will be counted
*-largestOverlap    Assign reads to a meta-feature/feature that has the largest number of overlapping bases.
*-t     Specify feature type in GTF annotation. `exon' by default.
*-g     Specify attribute type in GTF annotation. `gene_id' by default.
*-a     Name of an annotation file. GTF format by default.
*-o     Name of the output file including read counts.
*-O     Assign reads to all their overlapping meta-features
*-s     Perform strand-specific read counting. Possible values:
*           0 (unstranded), 1 (stranded) and 2 (reversely stranded).
*-f     Perform read counting at feature level (eg. counting reads for exons rather than genes).
*/


methods = ['gene', 'exon', 'transcript']


process count {

    scratch '/home/jp/RNA-seq/test'
    tag "$pair_id,$mode"
    publishDir "result/RNA-seq/$params.output_dir/$pair_id/count", mode: "copy"


    input:
    set pair_id, file (bam_file) from bam_count_gene
    file annotation from annotation_file
    each mode from methods

    output:

    file "${pair_id}_${mode}" into count_gene
    file "*.summary" into summary_gene


    """
    featureCounts -T ${task.cpus} \
    -p -M -O --largestOverlap -s 2 -f \
    -t ${mode} -g ${mode}_id \
    -a ${annotation} \
    -o ${pair_id}_${mode} ${bam_file} \

    """

}


workflow.onComplete {
    println ( workflow.success ? "Done!" : "Oops .. something went wrong" )
}
