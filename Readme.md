# Brain regional vulnerability to Parkinson’s disease.

This repository give online access to the code for the reproducibility of our results. It's linked to the submitted publication Transcriptomic signatures of brain regional vulnerability to Parkinson’s disease.

This is a joint work between
- [bilille](https://wikis.univ-lille.fr/bilille/), the bioinformatics, biostatistics and bioanalysis platform of the Lille metropolis
- [PGFS](http://www.ircl.org/genomique-figeac/), the genomic platform of Lille University (plateau de génomique fonctionnelle et structurale).

The nextflow script [RNA-seq.nf](RNA-seq.nf) does the QC, triming, alignment of reads and compute the counting matrix.
The differential analysis is performed with the R markdown script [analysis_sex_age_cond.Rmd](analysis_sex_age_cond.Rmd) with the DESeq2 tool.

For more details about the tools used and their version, please refer to the   [workflow_analysis](workflow_analysis.pdf) document.
